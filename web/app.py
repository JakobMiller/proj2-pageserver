from flask import Flask, render_template, request, abort, redirect

app = Flask(__name__)

@app.route("/") #root request for index page
def hello():
    check(request.path)
    return render_template("index.html")

@app.route("/trivia.html") #request for page 'trivia'
def trivia():
    check(request.path)
    return render_template("trivia.html")

@app.errorhandler(404) #requested page was not found
def missing(error):
    check(request.path)
    return render_template("404.html"), 404

@app.errorhandler(403) #requested page had a forbidden character
def forbiddenRequest(error):
    return render_template("403.html"), 403

def check(path): #Checks the passed in request string for forbidden requests
    for x in range(len(path)):
        if path[x] == '~':
            abort(403)
        if path[x] == '.' and x != len(path) -1:
            if path[x+1] == '.':
                abort(403)
        if path[x] == '/' and x != len(path) -1:
            if path[x+1] == '/':
                abort(403)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
